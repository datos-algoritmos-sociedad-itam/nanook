# Documento general Nanook

## Objetivos del proyecto

Cada año se invierte una cantidad considerable de recursos en campañas en favor de acciones contra el cambio climático, pero consistentemente resultan en poco alcance y aún menos concientización.

Es necesario identificar features de la relación entre imágenes, reacciones, organismos que lanzan la campaña, y variables contextuales para identificar los motivos o factores que logran mayor alcance y por tanto tienen mayor probabilidad de llevar a la acción.

## Descripción del proyecto

Se extraerán features de las imágenes, así como de sus reacciones para formar una matriz de diseño que posteriormente pueda ser ingresada a un modelo no supervisado para descubrir grupos emergentes y posteriormente perfilarlos y etiquetarlos. Finalmente se desarrollará un modelo supervisado precisamente para identificar estas etiquetas en el resto de las observaciones.

El entregable final será una webapp que cuente la historia del desarrollo del proyecto, sus conclusiones, y al final la posibilidad de cargar una fotografía y pasarla al modelo como nueva observación.

## Sección relacionada con la gráfica de temperatura y emisiones de carbono

En las primeras décadas del siglo pasado comienzan las discusiones en el ámbito académico y se reconoce el cambio climático como un fenómeno causado por las actividades humanas. En los 90's con la firma del protocolo de Kyoto se llevan tales discusiones al terreno político internacional, sin embargo, en el ámbito político se mantenía la duda de si realmente el cambio climático sería un problema. Actualmente aunque existen negacionistas, en la mayoría de los gobiernos sí se ha reconocido al cambio climático como un hecho. La mala noticia es que tiende a reconocer tardíamente las cosas. Esto lo observamos en la gráfica siguiente en la que se presenta un crecimiento tanto en emisiones como en temperatura, y donde puede verse que los países empiezan a tomar medidas más serias sólo cuando continúa el aumento de temperatura y ocurren fenómenos meteorológicos más serios.

![](https://i.imgur.com/HNUduDp.png)

Una buena noticia es que más allá de que el tema de cambio climático permee en la sociedad, ya está incorporándose en las leyes de varios países. Un ejemplo de lo anterior es el caso de Holanda, país en el que [el gobierno aplicó restricciones a Shell](https://elpais.com/clima-y-medio-ambiente/2021-05-26/la-justicia-de-paises-bajos-ordena-a-shell-que-reduzca-sus-emisiones-de-co-en-un-fallo-pionero.html), multinacional petrolera, por el daño que hizo al medio ambiente debido al exceso de producción de combustibles fósiles y dióxido de carbono. 

![](https://ichef.bbci.co.uk/news/1024/branded_news/9E50/production/_118682504_gettyimages-1228403501.jpg)
Fuente: BBC News

Otra buena noticia es que se han impulsado desde los inicios de los 2000's mecanismos como [REDD+ (Reducción de Emisiones por Deforestación y Degradación de bosques en países en desarrollo)](https://www.fao.org/redd/overview/en/) que buscan apoyar acciones para mitigar el cambio climático. Lo anterior se logra a través de la reducción de dióxido de carbono proveniente de uso de hidrocarburos, pero también evitando el cambio de uso de suelo, pues buena parte del cambio climático se debe a esto. Ejemplo del cambio de uso de suelo lo observamos en los 70's al talarse regiones completas cubiertas de vegetación en Tabasco, Veracruz, Chiapas y Campeche para abrir espacio a la ganadería. Cada tronco que se tala y quema es dióxido de carbono que se emite directamente a la atmósfera. Además, cada vaca produce 100 litros de metano al día aproximadamente, y cada molécula de metano es cuatro veces más poderosa como gas invernadero que el dióxido de carbono.

Por lo tanto el cambio climático no solamente es un asunto de los países industrializados quemando combustibles fósiles, también es un problema de países en vías de desarrollo, deforestando y talando sus ecosistemas forestales para ganadería y agricultura. En México y en varios otros países lo anterior fue particularmente drástico en la segunda mitad del siglo pasado. En México por ejemplo, se consideraban "tierras ociosas" a cualquier terreno que se dejara con vegetación natural en vez de abrirlo para ganadería o agricultura. Pero gracias al esfuerzo de REDD+ comenzó a pausarse y a disminuir la tasa de emisiones de carbono principalmente en Latinoamérica, excepto en Brasil. En Brasil aún continúa siendo un problema por el cultivo de la soya. Al contrastar las políticas públicas de la segunda mitad del siglo pasado con las de este siglo es sin duda una diferencia importante. En la siguiente gráfica el eje vertical son toneladas de CO2.

[![](https://i.imgur.com/23TWAUr.png)](https://data.worldbank.org/share/widget?end=2018&indicators=EN.ATM.CO2E.KT&locations=MX-BR&start=2000)

En México se tiene una historia de construcción sólida del sector ambiental. En 1992 se realizó la primera cumbre de la tierra en la que México presentó la iniciativa de consolidar una institución a nivel nacional que se encargaría de recuperar datos y generar conocimiento para tomar decisiones en torno a la biodiversidad, la Comisión Nacional para el Conocimiento y Uso de la Biodiversidad (CONABIO). También se fundó el [Instituto Nacional de Ecología y Cambio Climático (INECC)](https://www.gob.mx/inecc) en el mismo año. Aunque ha habido gobiernos no particularmente ambientalistas, México ha lidereado en América Latina en que los países de esta región se incorporen a los acuerdos internacionales y comiencen a tomar decisiones de política pública hacia temas del medio ambiente. Un ejemplo de lo anterior es que la frontera agropecuaria está legislada hoy en día en México, indicando que no puede talarse selva para abrir paso a la ganadería.

En México personajes como José Sarukhán actual director de la [CONABIO](https://www.gob.mx/conabio), Julia Carabias secretaria de la SEMARNAT en el gobierno de Zedillo, Alejandra Rabasa, integrante del Centro de Estudios Constitucionales, y Melissa Hernández de *Latina 4 Climate*, y muchas otras personas dedican  su vida a la conservación del medio ambiente.

![](https://imgur.com/rs8gMdt.png)
José Sarukhan, Julia Carabias, Alejandra Rabasa, Melissa Hernández

Entre los avances más recientes para México se encuentran el que en el 2012 [fue reformado el artículo 4º constitucional](https://www.cndh.org.mx/noticia/se-establece-en-la-constitucion-en-el-art-4o-el-derecho-de-toda-persona-un-medio-ambiente) que establece "Toda persona tiene derecho a un medio ambiente sano para su desarrollo y bienestar...", lo que ha resultado en llamados de la Suprema Corte de Justicia [para la protección del arrecife Veracruzano y de Holbox](https://www.cemda.org.mx/llaman-a-la-suprema-corte-a-proteger-el-arrecife-veracruzano/). Estos son triunfos importantes que sentan un precedente para otras áreas naturales protegidas.

A nivel global, históricamente no se han cumplido los objetivos de los acuerdos de cambio climático, pero sí hay países que han reducido las emisiones de carbono. En este sentido es importante señalar que en el ámbito internacional China y Estados Unidos han sido dos de los principales países en términos de la producción de gases de efecto invernadero y que hoy en día siguen jugando un papel muy importante. En un inicio estos países no se integraron a los acuerdos para reducir emisiones, pero recientemente han cambiado de opinión. Por ejemplo, [Estados Unidos ya se volvió a incorporar al acuerdo de París](https://www.state.gov/the-united-states-officially-rejoins-the-paris-agreement/). En la siguiente gráfica el eje vertical son toneladas de CO2.

[![](https://i.imgur.com/p8AkhSq.png)](https://data.worldbank.org/share/widget?end=2018&indicators=EN.ATM.CO2E.KT&locations=MX-BR-US-CN&start=2000)

Finalmente, el cambio climático es una realidad que indudablemente definirá el futuro de nuestro planeta. Si no se realizan acciones drásticas para frenarlo, nuestro estilo de vida cambiará radicalmente para mal. Es un problema mayor. Solamente la juventud puede entender que sí es posible realizar acciones drásticas para combatir al cambio climático. Es tarea de la juventud empujar que ocurran medidas radicales, porque son las personas jóvenes quienes no se creen el discurso de que "es muy difícil romper con prácticas que han llevado a esta situación ambiental". Para quienes quieran comenzar a aprender del tema, existen una gran cantidad de materiales que la juventud ha realizado. Un ejemplo de éstos se han creado a partir del movimiento [*Fridays For Future*](https://fridaysforfuture.org/) que en 2018 Greta Thunberg y otras personas activistas iniciaron y que está dirigida a la juventud. Aún estamos a tiempo de tomar las riendas del futuro y hacer los cambios radicales que se deben hacer.

Agradecimiento especial a la [Dra. Alicia Mastretta Yanes](https://mastrettayanes-lab.org/), catedrática CONACYT asignada a la CONABIO quien apoyó en la redacción de ésta sección.
