### Selenium

Selenium es una librería que te permite interactuar con las páginas web vía un navegador web. Esto permite scrapear de una manera cómoda sitios muy dinámicos.

**Table of Contents**

[TOC]

# Selenium y Facebook

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAeFBMVEUYd/L///8hevIAbfEAb/EAa/EAavEAcvKowvnH1/sJc/K90foSdfLh6/3R4Pymw/kwgfPr8f1Nj/R3pvZ/qva2zvrl7v3Y5fxhmfX0+P6Ns/ebvPhAiPRkm/Xy9/5XlPVCivScvfgzg/OqxvlvofWStvfa5vx9qfaQjqPBAAAKbklEQVR4nO3di3LiuBIGYBlZ1sGIqyEBEiAhJPv+b7i2gSEGX1pq/Razdbpqq3anNoRvLEtqXUUEj/3PcPW5zU6Hj+NgMBD5P8ePwynbfq6GP3v8rxfIDx+Pvk9Ho6SMtU6NEbcwJtU6llKZ4+l7NEZ+CZRwvFq+KxXfwR6joMZKvS9XKCZCOJ7uTI5rp91Bc6bZTRFK38LNKBMqTi1wt0hjJbLRxvM38ircrw5Suun+KKU8rLzWPx6Fq4OKbUpmU5hYHVb+vpYv4SSTXnhXpMwmnr6ZH+F0pniF8zFSNZt6+W4ehC9LP6XzPvLSunx5AuHrWmkA7xxarV8DCyeLxHfxrEaaLJgvJEv4ulCI4lkNoxas58gQjtcJ3lcakzWjs+Ms3GTeq8/mSFXm3NVxFU4lrn6pCy1d2w434WQGaR/awsQztyrHSZj19ALeGZOsJ+FI91tAb6H1qA/hTgXyFaF2cOFEhHqA59DC9m20FG6ToL4iki1QOD/GoX15xMc5SjixGnrBhdE2JdVC+Ba+hF4jeUMI1zK061fItXfhZha2Dr0PPaN2VInCccfAbv9hDDHfoAmH8tmAOVEO/Qm/gvRDu8IkX76EX89TiVaDRCQIn6iVuA9Kq9Et3IbsaXeF6u7CdQqfGkghdgnfnhuYE7sKaofwaSuZW3RVN+3CvwDYSWwVDv8GYE5sbfrbhOMn7MnUhZFtHbgW4ebp+qJNYUxLN7xFOPtbgDlx5iJcP1e61B66OV9sFL71m/AWbwTnrZCNzWKTcNJPNWpMuTKqXFmU/3v+H8UKqlhrY8tNmsZuGoTzPopoGiu9WE6Hr/M/FcVmP/+ZjL6+l+t3kyhps+xIN4zANQiP6FrGaGlO7Su9NuPh13Y9iIkjmOZoI9yCx0WN1EvyiOCUWJ7i+k54rRD8Ehr1YTPDMqL+dde/irVCrC852M3Lk4WiHlPzZztkNSOPtlMrdKGum5mqEY6AKaFR9pPVFs9Q1ZT+GiHwCcbvVpMq1kKhKcIMJ1RLe5+l8HEi/EGIq0eNIo1vsoQ19emDEJZRmPZE1ZPwMcu4F05hbb1yXZ9mJRTxfVV2J9zA0nrXJ2grNPIuG74TwqoZh1bCTfhQ2VSFY1RTqE/OQFuhUNX+fFW4Bi3Gaxtl8C5Mq/l+RfiKainUT49CkVT6vRXhAlTNNOQ1KKFZNAknsA4pB2gvrDZMv3856hFK3v4Qe2HlIf4Sot5CVjXjJKy8ib+EqIpUOiyZZAp/V6c34QvqLRzwgC5CoW5bUW7CJag7oz8DCPUtT7sJUY8w4e61cxEK9ShEJRXmwAS6CW8pxh8hKi+M2VsJnYS3CvwqhLX2ir0h1El4a/WvwgzV537nAh2F6TWJugpRc2n621q0n1fjy62GkFXhCjV4QVxAeInJ53oQS1UNx692rQAuwgNs8IK+I2u/FcrjSvJrJX4W7mFZBb1Ds008dzkuddxZCCukhrrHZT7w/hUuxfQshBVSTcx9fwCbUS/FtBRuYKsSYtow9xzyBc7jiqVwBBNK2jAwpkN1TttKIaq5fxjZa4g3TDVwbvRLIeTzyyD12XAD7VchbBw4fw8pzSFsrqQsQQL5G4RJKYUUtrSlTKEK4Q647ZwAnMNmLNPdRYhcHUQQwrobeRk6C4GvIanTtgVOq49LIfDvkCTc4cpQ0XETuEG2IijCd5ywGHIT0N9AEs5wv74YYRC4YcQiKMIB8PerQoisaMILx7nQbaCHGKGF8SgXfiPX6YUW6u9ciOzRBBfmvRqBXfAcWmiOuRC6Mya40EQCN8xWRGhhnqCKH+jOkeBC+SOG/3HhUCD73U8gjFfiE7o7JrhQfwpgdiaeQbgVuJHEIoIL00ycoDucggvNScCmLMoILzyID+TnhxfmviP088MLjwL7+eGFg/8LCZHGzUEZ1W/7+TyYNaGHv790Of1fcxCELT+dxxd7KJBttFtPYh/MDN1DKUULmZnB8wu5C3sH/PYQLOTmr0d+nwYs5E7ffvD7pWDhP7zsLu+XsnMLsJC5CSTPLdj5IVjIHOzM80N2jo8V7pmT/HmOzx6nwQpfmVWp/uSPtWGF3K8Xr/jjpVghd2ZMDvlj3lghd2ZM/vDnLbBC7qpFtefPPWGFzNewmHtizx9Chdx+dzl/eOKWdKSQWw+mJw/z+FAht99dzuNz12JAhdw+ZbkWg7ueBirkbr4u19Nw10RBhdw9NMrHujakkNtYX9a1MdcmIoUTbr976WN9KVLI73f7WCOMFHKT18saYeY6b6RwzaxoLuu8mf13pJC5uPbPWn1exwEp5L6G1/0WvBcRKOR2Rv7smeHtewIK2eMPUeRj7xpQyBwl+7V3jbX/kLjF0CWY/e5f+w95e0i1bAxVcxblQ2jV9OPc3Py2hxS3DzjkPP7vfcC4jUEhhZW93LCFwiGFlf34sGIaUFg9UwFWTAMK787FQJ1tElB4d7YJasN6OOHD+TSgM4bCCR/OGAIdaxBM+HhOFGjLejBhzVlfmI2WwYQ157VhtgOHEtaeuQc5NzGUsPbcRMjZl4GE9WdfQs4vDSRsOL8UcQZtGGHTGbSIVj+MsPEcYcBDDCJsPgsa8CYGEbac5+2/Og0hbDuT3f/5CiGErefqe78bIYCw/W4E78du9S/sut/Cd4rRv7DrjhLfeWLvwu57ZjzfFdS7kHBXkN/Kpm8h5b4nv3d29S58/HjwvWs9C4n3rvm8O69fIfXuPJ9nYfb8DGsxdX/orz7tVWhxh6W/e0j7FNrcQ+rvwM0ehXZ3yXq7D7hHoeV9wL5exf6Etnc6+7qXuzeh/b3cnu5W70vocre6nyyjJ2HblVItwo2Hk9z6ERrTcuR020G/Y37C34vQyLZz0VuPMh6yK9RehO0XgLYf1vzFJfYhTNpvJ+g4jppL7EHYAewSRm+8ZBEvVI0NIVEYbVlEuFB1XhHSfWg6i4gWdgMJwuiN8S6ChUlXEaUJOdUNVthVyZCFOdG16UcKDQlIE0ZD194NUGiIS+iJF9mOHfuoOKExpCtsyMJoM3NKpmBCPaNed0a/jHjtkhKjhLI5H3QXOrUaICGllXAQRhP7XbkQodE2m1isrsyeH23HURHC+NgwquZBWFzhF1yYWF7Ubnvt+URY1anehVrYbrOyv9h9Z9MT9y1U1PsUOcJopOmP0a9Qa4dLzB2EUZSR+6k+hSZ5nMImhJMwmsyIB6f6E5p45rbR0U0YRVNJKqrehFrer5Ohhqsw2mSKsMzPkzBVGf3W3btwFub5xrr7dfQiNMmamEfUBUMYRa+LritgPQiNWrx2f0hzsIR5lbNIWssqW5gmC+ZOaqYwf45r1VLnMIVarVnPrwi2MIpelqqx7eAITayWL90/3hUehHlMZw0Vq7swVTPX9qEafoT5C5nJugfpKDSxzHwdZOBLmMfq8FhaXYR56Tys/H0tj8Io2q8OUqYsYSrlYUW5JpkcXoV5bEaZUHHqJExjJbKRc+elIXwLixhPd0bF5aAOVWh0rMxuyui6NAZCWMR4tXxXShrC/2qkUu/LFUJXBEpYxnj0Tfi/vkcoXBlQ4VPEv+gtoSG5Z5ycAAAAAElFTkSuQmCC)

Se usó selenium para tratar de obtener las imágenes de los post de facebook, debido a que los datos provenientes de la dashboard de crowdtangle no venían con las respectivas imagenes de los post.

##¿Cuál era el plan?

Si analizamos detenidamente cómo esta construida la página de facebook, nos podemos dar cuenta que está bastante estructurada, por lo que es muy fácil obtener los elementos que queremos. 
Con esto, es posible obtener algunas imagenes con soló hacer un request a la URL del post, sin embargo, estas imagenes vienen escaladas de backend, por lo que si la visualización es pequeña, entonces también lo serán las imagenes que se descarguen de esos source. Y aún cuando queramos hacer un simple post, sólo obtendríamos como máximo 4 imagenes del post, pues ese es el límite de visualización previa de facebook. Por esto es que la solución que plantee usa el carousel que tiene facebook para sus imagenes, ya que el carousel escala las imagenes a su tamaño original y te deja "iterar" entre todas las imágenes de la publicación. Ese carousel es fácilmente utilizable por selenium.

## Pseudocódigo

1. Obtener la primer url del csv.
2. Abrir el url con selenium.
3. Buscar el elemento tal que su xpath sea:
~~~
//div[@data-testid='post_message']/following-sibling::div//a
# Pues el div posterior al mensaje del post es siempre el de las imagenes
~~~
4. Declarar una variable "cantidad de imagenes" con valor igual a la cantidad de anchors encontrada.
5. Preguntar si la variable es igual a 4, en caso afirmativo continuar al punto 5, en caso contrario ir al punto 17.
6. Preguntar si existe un elemento tal que su xpath sea:
~~~
/div/following-sibling::div/div/div
~~~
En caso afirmativo ir al punto 7, de lo contrario ir al punto 8.
7. Extraer el texto del div y sumarlo a la cantidad de imágenes.
8. Darle click al primer anchor
9. Declarar una variable "contador" igual a 0 y una variable "arreglo imagenes" en la cuál meter las urls de cada imagen.
10. Preguntar si contador < 'cantidad de imagenes', caso afirmativo seguir al punto 11, de lo contrario ir al punto 17.
11. Buscar el elemento tal que su xpath sea:
~~~
//img[@class='spotlight']
~~~
12. Obtener el src de ese elemento y agregarlo al arreglo de urls.
13. Buscar el elemento tal que su xpath sea:
~~~
//a[contains(., 'snowliftPager next') and @role='button']
~~~
14. Darle click a ese elemento
15. contador++
16. Regresar al punto 10.
17. Fin
## Código en Python
```python
import pandas as pd
import numpy as np
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import requests
from pathlib import Path
import urllib.request
from selenium.common.exceptions import NoSuchElementException
df = pd.read_csv("example.csv")
url_col = df["URL"].values
first_element = url_col[0]
images_urls = []
extra_images = 0
driver = webdriver.Safari()
driver.maximize_window()
driver.get(first_element)
page_source = driver.page_source
f = open("page_source.html", "w")
f.write(page_source)
f.close()
anchors = WebDriverWait(driver, timeout=5).until(lambda d: d.find_elements(By.XPATH, "//div[@data-testid='post_message']/following-sibling::div//a"))
quantity_of_images = len(anchors)
print(quantity_of_images)
if quantity_of_images > 0:
    if quantity_of_images == 4:
        try:
            element_with_extra = anchors[-1].find_element(By.XPATH, "/div/following-sibling::div/div/div")
            extra_images = element_with_extra.text[1:]
            print(extra_images)
            quantity_of_images += int(extra_images)
        except NoSuchElementException:
            print("No such element")
    anchors[0].click()
    for i in range(0, quantity_of_images):
        image = WebDriverWait(driver, timeout=5).until(lambda d: d.find_element(By.XPATH, "//img[@class='spotlight']"))
        images_urls.append(image.get_attribute("src"))
        nextButton = driver.find_element(By.XPATH, "//a[contains(., 'snowliftPager next') and @role='button']")
        nextButton.click()
for x in range(len(images_urls)):
    path = "scroll_imgs/"+first_element.split("/")[-1]+"/"
    p = Path(path)
    p.mkdir(parents=True, exist_ok=True)
    urllib.request.urlretrieve(images_urls[x], (path + str(x) + ".jpg"))
    print (images_urls[x])
driver.quit()
```
El código tiene algunas extras para debuggear correctamente, y un último ciclo para descargar las imagenes de las urls usando urllib. Este código solo usa la primera url del csv, sin embargo, se puede adaptar facilmente para iterar en la lista entera.
## ¿Por qué no funcionó?
Cuándo facebook te detecta como bot te prohíbe el acceso a las publicaciones, así estas sean públicas, sin autenticarte. Pero si lo haces con una cuenta registrada, al detectarte como bot banean la cuenta. Podrían añadirse cosas al código para evitar ser detectados, aunque no lo recomendaría pues es una práctica que va en contra de las políticas de facebook, lo cuál acarrería otros problemas.
Por esto yo recomendaría hacer cualquiera de las siguientes propuestas:
- Pedir permiso explícito a facebook y que ellos te faciliten los endpoints
- Utilizar la api de crowdtangle para la descarga de imágenes