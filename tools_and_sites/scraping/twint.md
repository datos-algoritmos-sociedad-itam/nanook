# Twint

Twint nos permite hacer scrapping de tweets; sin embargo, no nos deja descargar imágenes y parece que, al intentarlo usar en Python, suele fallar.

Ejemplo de ejecución en la terminal con un script de bash:

El primer bloque es en donde se usa Twint: con -u se indica que se desean obtener todo los tweets del perfil que se encuentra entre comillas pero por algún motivo si no se pone el --since, nos regresa una gran cantidad de tweets pero no todos los de la cuenta.
Con -o se indica el nombre del output (el archivo donde se guardaran los tweets) y con --json que se desea que los tweets sean guardados en formato json.

```bash
#!/bin/bash

 twint -u "UNFCCC" --since "2000-01-01" -o unfccc.json --json
 twint -u "theGCF" --since "2000-01-01" -o the_gcf.json --json
 twint -u "ScienceMagazine" --since "2000-01-01" -o science_magazine.json --json
 twint -u "sciam" --since "2000-01-01" -o sciam.json --json
 twint -u "NASA" --since "2000-01-01" -o nasa.json --json
 twint -u "CarbonBrief" --since "2000-01-01" -o carbon_brief.json --json
 twint -u "WWF" --since "2000-01-01" -o wwf.json --json
 twint -u "360org" --since "2000-01-01" -o 360org.json --json
 twint -u "Greenpeace" --since "2000-01-01" -o greenpeace.json --json
 twint -u "Fridays4future" --since "2000-01-01" -o fridays4future.json --json
 twint -u "Earthjustice" --since "2000-01-01" -o earthjustice.json --json
 twint -u "UCSUSA" --since "2000-01-01" -o ucsusa.json --json
 twint -u "friends_earth" --since "2000-01-01" -o friends_earth.json --json

 #appending all jsons in one
cat *.json > tweets.json

#removing individual jsons
rm -f unfccc.json
rm -f the_gcf.json
rm -f science_magazine.json
rm -f sciam.json
rm -f nasa.json
rm -f carbon_brief.json
rm -f wwf.json
rm -f 360org.json
rm -f greenpeace.json
rm -f fridays4future.json
rm -f earthjustice.json
rm -f ucsusa.json
rm -f friends_earth.json
```


Ejemplo en python

Este script debería de devolver `100` tweets del prefil `nasa` que contengan la palabra `climate` desde la fecha `2000-01-01`, sin embargo, solo nos regresa informacion del perfil. 

***Un error de Twint es que el Bearer_token por default esta en `NULL` por lo que si no se configura el script no se ejecuta.***

```python
import nest_asyncio
nest_asyncio.apply()
import twint

#Twint config
c = twint.Config()

twint.token.Token(c).refresh()
#sin Bearer_token no funciona (!!!no aparece en el git)
c.Bearer_token = twint.run.bearer
c.Search = "climate"
c.Username = "nasa"
c.Since = "2000-01-01"
c.Output = "nasaP.json"
c.Store_json = True
c.Images = True
c.Hide_output = False
c.Limit = 100
twint.run.Lookup(c)
```
