# Minet:
Una herramienta para poder scrappear datos de páginas de internet, el link para
el repositorio es el siguiente:

  [Github Minet](https://github.com/medialab/minet)

El uso de la herramienta fue mediante la terminal y la utilicé para dos redes
sociales, twitter y facebook. Para más información sobre la herramienta y sus
usos, diríjase al siguiente link:

  [Información de Comandos](https://github.com/medialab/minet/blob/master/docs/cli.md)

Ahora veamos los resultados para facebook y twitter:

  * Facebook: Hay un problema con facebook y es que solo permite descargar posts
  de grupos y es necesario contar con una cuneta, otro problema es que no hay un
  filtro para solo descargar imágenes y el reporte que genera contiene mucha
  información un tanto complicada para limpiar y por último, no cuenta con una
  opción de límite de posts. El reporte que porporciona es un reporte donde viene
  la información del usuario creador del posts, url del posts, texto del post,
  número de reacciones y número de comentarios
  Los comandos usados en la terminal fueron:
```
    $ minet fb posts https://www.facebook.com/groups/266398937146537 > postsPb.csv
    $ minet url-parse url postsPb.csv --facebook > report.csv
```

  - Twitter: Con twitter se logró la descarga de imágenes según un hashtag
  proporcionado y permite poner un filtro para saber qué descargar y también un
  límite de imágenes que lo descarga a un reporte con información similar a la
  de facebook y además crea un folder con las fotos descargadas. En esta prueba
  se usaron los siguientes comandos en la terminal:
```
    $ minet twitter scrape tweets "#BreakFreeFromPlastic filter:images" --limit 10 > tweets.csv
    $ minet fetch media_urls tweets.csv \
      >   --separator "|" \
      >   -d images \
      >   --throttle 0 \
      >   --domain-parallelism 5 > report.csv
```

# Shutterscrape:
Esta herramienta descarga imágenes de shutterstock, su uso fue mediante python
spyder. Un problema con esta herramienta es que descarga las imágenes con el título
proporcionado en la página, entonces puede llegar a confundir y no saber con
claridad la información de la imagen, otro problema es que los metadatos son creados
cuando se descarga la imagen, entonces se pierde la fecha de cuando se subió la
imagen a la página web. La herramienta se puede consultar en el siguiente
repositorio:

  [Github Shutterscrape](https://github.com/chuanenlin/shutterscrape)

* El comando utilzado fue el siguiente:

```
    Please select a directory to save your scraped files
    Search mode ('v' for video or 'i' for images): i
    Select image type ('a' for all or 'p' for photo): p
    Number of search terms: 2
    Search item 1: climate
    Search item 2: change
    Number of pages to scrape: 2
```

# Flickr Scraper
Esta herramienta descarga imágenes de la página web flickr, su uso fue mediante
la terminal. Un problema con esta herrramienta es que descarga imágenes pero los
metadatos son creados cuando se descarga la imagen, entonces no es posible saber
cuando se subió a la página web, otro porblema es la necesidad de una API key.
La herramienta se puede consultar en el siguiente
repositorio:

  [Github flickr Scraper](https://github.com/ultralytics/flickr_scraper)

* El comando utilizado fue:

  ```
      python3 flickr_scraper.py --search '350org' --n 10 --download
  ```
