from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Análisis de imágenes históricas en campañas, medios y comunicación oficial relacionada con la concientización y difusión de hitos sobre el cambio climático en los últimos 20 años, tanto en contenido como en viralidad y reacciones.',
    author='Centro ITAM para Datos + Algoritmos + Sociedad',
    license='MIT',
)
