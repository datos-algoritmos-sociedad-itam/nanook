Nanook
==============================

Análisis de imágenes históricas en campañas, medios y comunicación oficial relacionada con la concientización y difusión de hitos sobre el cambio climático en los últimos 20 años, tanto en contenido como en viralidad y reacciones.

**Partner: SocialTIC**

Párrafo de prueba

Justificación
-------------

Cada año se invierte una cantidad considerable de recursos en campañas en favor de acciones contra el cambio climático, pero consistentemente resultan en poco awareness y aún menos concientización.

Es necesario identificar features de la relación entre imgs, reacciones, organismos que lanzan la campaña, y variables contextuales para identificar los motivos o factores que logran mayor awareness y por tanto tienen mayor probabilidad de llevar a la acción.

Planteamiento de la Solución
----------------------------

Se extraerán features de las imágenes, así como de sus reacciones para formar una matriz de diseño que posteriormente pueda ser ingresada a un modelo no supervisado para descubrir grupos emergentes y posteriormente perfilarlos y etiquetarlos. Finalmente se desarrollará un modelo supervisado precisamente para identificar estas etiquetas en el resto de las observaciones.

El entregable final será una webapp que cuente la historia del desarrollo del proyecto, sus conclusiones, y al final la posibilidad de cargar una fotografía y pasarla al modelo como nueva observación.

Actividades:
------------

-   Selección de alcance de campañas, medios e instituciones a analizar

-   Descarga y documentación de metadatos de imágenes 

-   Análisis de características de imágenes por fuente, medio y metadatos

-   Contraste de resultados del análisis contra hitos y mensajes coyunturales del cambio climático en los últimos 20 años

-   Desarrollo de modelos de agrupación para descubrimiento de features

-   Desarrollo de modelos de clasificación para asignación de etiquetas

-   Visualización comparativa de resultados entre elementos de similitud

-   Visualización descriptiva de características por tipo de representación del cambio climático

-   Desarrollo de micrositio web con la narrativa de la investigación y los resultados.

Recursos
--------

-   [Top 10: climate change campaigns | Working in development | The Guardian](https://www.theguardian.com/global-development-professionals-network/2013/nov/15/top-10-climate-change-campaigns)

-   [(PDF) Communicating climate change: Improving the effectiveness of public campaigns (researchgate.net)](https://www.researchgate.net/publication/265207143_Communicating_climate_change_Improving_the_effectiveness_of_public_campaigns)

-   <https://ec.europa.eu/environment/archives/cbn-e/doc/conf-feb07/bhaskar.pdf>

Equipo
------

-   Quién lleva el kanban board (PM)?

-   Quién le dará principalmente a la tecla (Data Scientists)?

-   Quienes le darán a la tecla part-time (Data Scientists)?

-   Quién revisa que la tecla esté bien (Mentor Técnico)?

-   Quién revisa que la tecla se apegue al problem domain (Business Specialist)?

Project Organization
------------

**Es imperativo respetar las convenciones de esta estructura de folders** para facilitar el seguimiento de proyectos.

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
