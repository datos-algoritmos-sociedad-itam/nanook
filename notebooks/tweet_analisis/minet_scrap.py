import pandas as pd
import numpy as np
import os
import argparse
parser = argparse.ArgumentParser(description='Scrap minet!')
parser.add_argument('-c', '--csv', type=str, help='Csv to use for scraping')
parser.add_argument('-d', '--dir', type=str, help='Directory name to use for saving the results')
parser.add_argument('-l', '--limit', type=str, help='Max quantity of tweets for each query')
args = parser.parse_args()
if args.csv is not None and args.dir is not None and args.limit is not None:
    df_hashtags = pd.read_csv(args.csv, header=None)
    working_directory = os.getcwd()
    if not os.path.exists(args.dir):
        os.makedirs(args.dir)
    wdif = working_directory + "/" + args.dir
    os.chdir(path=wdif)
    for hashtag in df_hashtags.loc[:].values[:]:
        os.system('minet twitter scrape tweets \"' + hashtag[0] + ' filter:images\" --limit ' + args.limit + '  > tw_' + hashtag[0][1:] + '.csv')
        os.system('minet fetch media_urls tw_' + hashtag[0][1:] + '.csv --separator \"|\" -d images --throttle 0 --domain-parallelism 5 > rep_' + hashtag[0][1:] + '.csv')
    os.chdir(path=working_directory)
else:
    print("One or more args are missing") 
