#saving headers
head -1 rep_ClimateChangeIsReal.csv > h_report.csv
head -1 tw_ClimateChangeIsReal.csv > h_tweets.csv

#removing headers
sed -i '1d' rep*
sed -i '1d' tw*

#appendig all reports
cat rep* >  auxrep.csv

#appending all tweets
cat tw* > auxtw.csv

#adding headers
cat  h_report.csv auxrep.csv | tee freport.csv
cat h_tweets.csv auxtw.csv | tee ftweet.csv

#removing auxiliar files 
rm aux*

#removing individual files and header files
rm rep*
rm tw*
rm h*
