import pandas as pd
import flickr_api as flickr
import os
import traceback
import requests
from urllib.request import urlopen as uReq
import argparse

parser = argparse.ArgumentParser(description='Scrap Flickr!')
parser.add_argument('-c', '--csv', type=str, help='csv file with keys')
parser.add_argument('-d', '--dir', type=str, help='Directory name to use for saving the results')
parser.add_argument('-l', '--limit', type=int, help='Max quantity of albums right now max is 501')
args = parser.parse_args()
if args.csv is not None and args.dir is not None and args.limit is not None:
    
    df_keys = pd.read_csv(args.csv, header=None)
    flickr.set_keys(api_key = df_keys.loc[0].values[0], api_secret = df_keys.loc[1].values[0])
    num_pags=args.limit
    os.chdir("..")
    working_directory = os.getcwd()
    if not os.path.exists(args.dir):
        os.makedirs(args.dir)
    wdif = working_directory + "/" + args.dir
    os.chdir(path=wdif)
    
    user = flickr.Person.findByUserName("350.org")
    albums = user.getPhotosets(user_id='25654955@N03',extras= 'date_taken, views') 
    array_terminations=["jpg","jpeg", "gif", "png"]
    array_images=[]
    filename = "reporte350org.csv"
    f = open(filename, "w")
    headers = "img_url, date, title, views\n"
    f.write(headers)
    os.mkdir('images 350org')
    
    for i in range(num_pags):
        try:
            album = albums[i]
            album_title = album['title']
            photos = album.getPhotos(user_id='25654955@N03',extras= 'date_taken, views, url_c')
            for j in range(len(photos)):
                try:
                    photo = photos[j]
                    views = photo['views']
                    date = photo['datetaken']
                    img = photo['url_c']
                    f.write(img+ ","+ date.split(" ")[0]+"," +album_title.replace(","," ").replace("\n", " ") + "," +str(views)+ "\n")
                    array_images.append(img)
                except:
                    pass
        except:
            pass

    f.close()

    for image in array_images:
        name=image.split('/')[-1]    
        if not name.split(".")[-1] in array_terminations:
            name=name+".jpg"
        os.chdir(path=wdif+'/images 350org')
        img_data = requests.get(image).content
        with open(name, 'wb') as handler:
            handler.write(img_data)
        os.chdir(path=wdif)
else:
    print("One or more args are missing")

