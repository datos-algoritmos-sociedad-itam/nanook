import bs4
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup
import os
import traceback
import requests
import argparse
parser = argparse.ArgumentParser(description='Scrap NASA!')
parser.add_argument('-d', '--dir', type=str, help='Directory name to use for saving the results')
parser.add_argument('-l', '--limit', type=int, help='Max quantity of pages, right now max is 30')
args = parser.parse_args()
if  args.dir is not None and args.limit is not None:


    num_pags=args.limit
    os.chdir("..")
    working_directory = os.getcwd()
    if not os.path.exists(args.dir):
        os.makedirs(args.dir)
    wdif = working_directory + "/" + args.dir
    os.chdir(path=wdif)
    
    array_terminations=["jpg","jpeg", "gif", "png"]
    array_images=[]
    filename = "reporteNASA.csv"
    f = open(filename, "w")
    headers = "img_url, date, title\n"
    f.write(headers)
    os.mkdir('images NASA')
    for i in range(num_pags):
        try:
            my_url='https://climate.nasa.gov/ask-nasa-climate/'+"?page="+str(i+1)
            uClient=uReq(my_url)
            page_html = uClient.read()
            uClient.close()
            page_soup=soup(page_html, "html.parser")
            containers = page_soup.findAll("article",{"class":"blog_entry"})
            for container in containers:
                title = container.a.text
                date = container.span.text.strip()
                article_url="https://climate.nasa.gov"+container.a['href']
                subuClient=uReq(article_url)
                subpage_html = subuClient.read()
                subuClient.close()
                subpage_soup=soup(subpage_html, "html.parser")
                subcontainers = subpage_soup.findAll("figure")
                for subcontainer in subcontainers:
                    try:
                        img="https://climate.nasa.gov"+subcontainer.img['src']
                        f.write(img+ ","+ date.replace(","," ").replace("\n", " ")+"," +title.replace(","," ").replace("\n", " ") + "\n")
                        array_images.append(img)         
                    except:
                        pass
        except Exception as e:
            print(traceback.format_exc())
        
    f.close()

    for image in array_images:
        name=image.split('/')[-1]    
        if not name.split(".")[-1] in array_terminations:
            name=name+".jpg"
        os.chdir(path=wdif+'/images NASA')
        img_data = requests.get(image).content
        with open(name, 'wb') as handler:
            handler.write(img_data)
        os.chdir(path=wdif)
else:
    print("One or more args are missing")